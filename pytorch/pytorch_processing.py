import cv2 as cv
from imutils.perspective import four_point_transform
from imutils import contours
import imutils

import numpy as np
import torch # pip
import torchvision # pip
import matplotlib.pyplot as plt
from time import time
from torchvision import datasets, transforms
from torch import nn, optim

## Pytorch Processing

transform = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5,), (0.5,)),])


trainset = datasets.MNIST('/training sets', download=True, train = True, transform = transform)
valset = datasets.MNIST('/training sets', download=True, train = False, transform = transform)

trainloader = torch.utils.data.DataLoader(trainset, batch_size=64, shuffle=True)
valloader = torch.utils.data.DataLoader(valset, batch_size=64, shuffle=True)

dataiter = iter(trainloader)
images, labels = dataiter.next()

print("hey there handsome")

sample_image = images[0].numpy().squeeze()
plt.imshow(sample_image, cmap='gray_r')
print("size is {}".format(sample_image.shape))
plt.show()

figure = plt.figure()
num_of_images = 60
for index in range(1, num_of_images + 1):
	plt.subplot(6, 10, index)
	plt.axis('off')
	plt.imshow(images[index].numpy().squeeze(), cmap='gray_r')



## Building the Neural Network

input_size = 784
#hidden_sizes = [128, 64]
hidden_sizes = [512,256]
output_size = 10

model = nn.Sequential(
	nn.Linear(input_size, hidden_sizes[0]), \
	nn.ReLU(),\
	nn.Linear(hidden_sizes[0], hidden_sizes[1]),\
	nn.ReLU(),\
	nn.Linear(256, 128),\
	nn.ReLU(),\
	nn.Linear(128, 64),\
	nn.ReLU(),\
	nn.Linear(64, 32),\

	#nn.Linear(hidden_sizes[0], hidden_sizes[1]), \
	nn.ReLU(),\
	#nn.Linear(hidden_sizes[1], input_size),\
	nn.Linear(32, 10),\
	nn.LogSoftmax(dim=1))


criterion = nn.NLLLoss()
images, labels = next(iter(trainloader))
images = images.view(images.shape[0], -1)

logps = model(images) # log probabilities
loss = criterion(logps, labels) # calculate NLL loss


# training

optimizer = optim.SGD(model.parameters(), lr=0.003, momentum=0.9)
time0 = time()
epochs = 15
for e in range(epochs):
	running_loss = 0
	for images, labels in trainloader:
		# flatten NMIST images into 784 long vector
		images = images.view(images.shape[0], -1)

		# Training pass
		optimizer.zero_grad()

		output = model(images)
		loss = criterion(output, labels)

		# model learns
		loss.backward()

		# optimizes weights
		optimizer.step()

		running_loss += loss.item()
	else:
		print("Epoch () - Training loss: ()".format(e, running_loss/len(trainloader)))

print("\nTraining Time (in minutes) =", (time() - time0)/60)


correct_count, all_count = 0, 0
for images, labels in valloader:
	for i in range(len(labels)):
		img = images[i].view(1, 784)
		with torch.no_grad():
			logps = model(img)

		ps = torch.exp(logps)
		probab = list(ps.numpy()[0])
		pred_label = probab.index(max(probab))
		true_label = labels.numpy()[i]
		if(true_label == pred_label):
			correct_count += 1
		all_count += 1

print("Number Of Image Tested =", all_count)
print("\nModel Accuracy ", (correct_count/all_count))


torch.save(model, './my_mnist_model.pt')