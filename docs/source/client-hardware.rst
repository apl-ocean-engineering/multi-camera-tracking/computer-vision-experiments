Client Hardware
========================

*The following instructions will teach you how to set up the hardware aspect of the client devices for this project*

The clients for this project are multiple Raspberry Pi devices with the camera module installed. 

Out-of-the-box Pi
++++++++++++++++++

To initially boot up the Raspberry Pi, follow the setup instructions on the `official Raspberry Pi webpage <https://projects.raspberrypi.org/en/projects/raspberry-pi-setting-up>`_, then plug in the
power supply and an HDMI cable to a monitor. Also needed are a USB keyboard and mouse to control input and movement on the screen.

Attaching the Camera Module
++++++++++++++++++++++++++++++

Similarly, the camera module can be installed by following the instructions on the `official Raspberry Pi webpace <https://projects.raspberrypi.org/en/projects/getting-started-with-picamera>`_.



Test whether the camera is attached by opening up a terminal window and running the command ``vcgencmd get_camera``. The output should be ``supported=1, detected=1``. If the value for ``detected`` is 
``0``, then the camera hardware may be faulty and should be replaced.

Initial Configuration
++++++++++++++++++++++++++++

The following steps are required to configure the pi system before it can be used as a client.

#. Connect to a wifi network. The Pi should prompt the user to connect to one during the first boot, but if that step was skipped then, it is required now.
#. Note down the ip address of the Pi. This can be done by opening a terminal window and running the command ``ifconfig`` and reading the ``inet addr`` listed under the type of network connection.
#. Enable SSH on the pi by going to Main Menu > Preferences > Raspberry Pi Configuration > Interfaces and select "enabled" next to SSH. Reboot the pi for changes to take effect. For security, it's recommended to change the default password when enabling SSH.
#. Change the host name of the Raspberry Pi by going to Main Menu > Preference > Raspberry Pi Configuration. This will make it easier to differentiate the Pi's later, especially when there are more connected to the server.

