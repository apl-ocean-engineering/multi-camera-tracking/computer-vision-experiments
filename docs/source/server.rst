Server
========================

*The following instructions will teach you how to set up the server device for this project*

The server is the device which will be connecting to each individual client, sending out commands, and receiving and processing images.
A computer or laptop with decent processing power is recommended.

Installing Python3
++++++++++++++++++++++++++++

The code for this project was written in Python 3.7+. To install Python on your device,
select the version for your operating system from the `official Python website <https://www.python.org/downloads/>`_.

.. note :: If you choose to use a virtual environment such as Conda for the installation of python packages (recommended unless you have a device whose sole use is to act as a server for this project), you must keep track of where the Python executable is installed

.. 
	[Q] Do I include a section on virtual environments and how to set one up?

Installing Python Packages
++++++++++++++++++++++++++++

A few python packages are required by the server for this project. Versions from 3.4+ of Python should come bundled with the pip program. Using pip makes it much easier to
install all the packages we need. 

In the command prompt/terminal, run ``pip install <package name>``, where <package name> is the name of the package. Each of the package names needed for the server scripts are listed below.

* opencv-contrib-python
