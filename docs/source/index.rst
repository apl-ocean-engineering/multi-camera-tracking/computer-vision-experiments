.. Underwater Motion Capture System documentation master file, created by
   sphinx-quickstart on Mon Sep 13 20:45:26 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

An Underwater Motion Capture System That Does Not Cost $150,000
=======================================================================

Version 1.0.0

**Marvin's summer internship project at the University of Washington's Applied Physics Laboratory**

Underwater tracking of ROV submersibles is difficult. On-board sensors are often inaccurate and GPS signal does not reach the depth that the ROVs operate at. Because of this, using external cameras to track and locate objects deep underwater is an effective method. However, since the only commercially available underwater camera system costs a LOT of money, this project is meant to be an affordable alternative.

 


Setting Up
==================


.. toctree ::
	:caption: Installation and environment setup for both the server and clients are explained here:

	server
	client-hardware
	client-software



Running the Script
======================


	#. Run ``threading_server.py`` on the server device.
	#. Run ``threading_client.py`` on each client device. Each client and the server should show a "connected" status message.
	#. When prompted, type "image" on the server task window to request an image, or type "quit" to terminate the program
	#. Received images will be in the four_way_images folder
	#. Once images are received, user will be prompted once more for input, looping back to step 3
|

Additional Information:
========================


.. toctree::
	:caption: This project is licensed under the BSD License:

	license





..
	* :ref:`genindex`
..
	* :ref:`search`
