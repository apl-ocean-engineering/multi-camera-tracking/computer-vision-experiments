clear all; close all;

%% Single Tag

%t = [0.0174301  0.0028422  0.05940885];
%r = [2.9629581   0.12239852 -0.75124631];

r = [-2.86342771 -0.13636096 -1.11195602];
t = [-6.55662474  0.415457   24.24864316];
    
drawSingleCameraPose(t, r);
%drawCameraPoses(r1,t2,true);



%% All Tags


r = [2.96295825  0.12239849 -0.75124633; ...
    3.00848207  0.11256226 -0.76059138; ...
    2.9805002   0.00597047 -0.83662852; ...
    3.02289108 -0.04816513 -0.74383699; ...
    3.08618888 -0.13177708 -0.50906239; ...
    3.09441174 -0.12912013 -0.43646247; ...
    3.09478102 -0.1586875  -0.34155359; ...
    3.06339297 -0.16309349 -0.26699004; ...
    3.09874581 -0.09778158 -0.23288034; ...
    3.05094336 -0.07788528 -0.06967007; ...
    3.03291282 -0.08061784  0.31918567; ...
    2.91491528 -0.019473    0.67225459; ...
    -2.81347625  0.00717985  1.38338988; ...
    2.85107372  0.00393499 -1.21766758; ...
    2.89815764  0.05820702 -0.94396426; ...
    -2.50868812 -0.14114169 -1.69575477; ...
    -2.81479603 -0.09155701 -1.33544723; ...
    -2.86342771 -0.13636096 -1.11195602; ...
    -2.87044182 -0.10444544 -0.94661564; ...
    3.05961932 0.09781503 0.62329848; ...
    -2.95637129 -0.08970798 -0.78434967; ...
    -3.06422415 -0.16199298 -0.52509206; ...
    3.07649569 0.13655024 0.56568778; ...
    3.11757369 0.14903207 0.31199514; ...
    -3.12519748 -0.187437    0.08942076; ...
    3.011228   0.201674  -0.3569803];


t = [8.2357238   1.34293816 28.07068245; ...
    6.61053883  1.86244296 25.07647581; ...
    4.71531102  0.50569098 24.81825107; ...
    3.70984694  0.08555938 25.14546796; ...
    3.10195238 -0.332253   25.57043815; ...
    2.16097829 -0.31838195 24.90068418; ...
    1.04196556 -0.69817338 24.71656244; ...
    -0.17109379 -1.30566578 27.63578347; ...
    -1.53133716  0.24140038 26.00368844; ...
    -4.90908214  1.15723    25.077695619; ...
    -5.88319664  0.71576306 26.13270193; ...
    -8.68397286 -0.19143791 24.28197411; ...
    5.71908161 -0.53256469 28.09412246; ...
    1.46558049  0.50975843 28.25108622; ...
    2.39917953 -0.42368377 31.74777843; ...
    -6.87780653  1.68875146 21.8658473; ...
    -7.48259439  0.33771742 24.40858425; ...
    -6.55662474  0.415457   24.24864316; ...
    -5.56785811  0.40992181 24.24370059; ...
    -4.9004207  -1.4417122  26.33743205; ...
    -10.66648958  -1.5808956   33.30889452; ...
    -8.50937493 -2.37833357 33.94323208; ...
    -6.7826507  -2.33221985 34.58193979; ...
    -4.50774019 -3.18536648 36.88505752; ...
    0.81068641 -1.31086698 35.48691531; ...
    1.59002334 -1.09770136 35.65427166];


drawCameraPoses(t,r);
%drawCameraPoses(r1,t2,true);


%function drawCameraPoses(rvec, tvec, rvec2, tvec2, flag)
function drawCameraPoses(tvec, rvec)

%plot camera positions
figure ;

% Plot the camera as origin
plot3(0,0,0,'b+');

%watch out xyz order
hold on ;

num_rows = size(tvec,1);
baseSymbol = 30 * [0 1 0 0 0 0;0 0 0 1 0 0;0 0 0 0 0 1] ;

for i = 1:num_rows
    tvec_i = tvec(i,:);
    plot3(tvec_i(:,1), tvec_i(:,2), tvec_i(:,3),'r+');
    
    rvec_i = rvec(i,:);
    for j = 1 : size(rvec_i,1)
       rotM = rod2angle(rvec_i(j,:));
       baseK = rotM * baseSymbol + tvec_i(j,:)' * ones(1,6) ;
       plot3(baseK(1,:),baseK(2,:), baseK(3,:),'-b') ;
    end
end

grid on;

xlim([-60 60]) ;
ylim([-60 60]) ;
zlim([0 60]) ;

end

%% DrawSingleCameraPose

%function drawSingleCameraPose(rvec, tvec, rvec2, tvec2, flag)
function drawSingleCameraPose(tvec, rvec)

%plot camera positions
figure ;

% Plot the camera as origin
plot3(0,0,0,'b+');

%watch out xyz order
hold on ;

plot3(tvec(:,1), tvec(:,2), tvec(:,3),'r+');


grid on;

xlim([-60 60]) ;
ylim([-60 60]) ;
zlim([0 60]) ;

%plot camera orientation
baseSymbol = 30 * [0 1 0 0 0 0;0 0 0 1 0 0;0 0 0 0 0 1] ;
for i = 1 : size(rvec,1)
   %rotM already transposed
   %rotM = angle2rod(rvec(i,1), rvec(i,2), rvec(i,3)) ;
   %rotM = rvec(i,:);
   rotM = rod2angle(rvec(i,:));
   baseK = rotM * baseSymbol + tvec(i,:)' * ones(1,6) ;
   disp(baseK)
   plot3(baseK(1,:),baseK(2,:), baseK(3,:),'-b') ;
end

end
