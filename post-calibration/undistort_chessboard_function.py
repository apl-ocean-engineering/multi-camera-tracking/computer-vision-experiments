import cv2 as cv

PATH = "calibration_chessboard.yaml"

cv_file = cv.FileStorage(PATH, cv.FILE_STORAGE_READ)

camera_matrix = cv_file.getNode('K').mat()
dist_matrix = cv_file.getNode('D').mat()

cv_file.release()

# print(camera_matrix)
# print(dist_matrix)

original = cv.imread("images/online_calibration_images/camera_calib23.jpg")
dst = cv.undistort(original, camera_matrix, dist_matrix, None, None)

# blend = cv.addWeighted(original, 0.3, dst, 1, 0)
# cv.imshow("blended", blend)

cv.imshow("hehehe", dst)
cv.imshow("original", original)
cv.waitKey(0)
