import cv2.aruco as A
import cv2 as cv
import pathlib

PATH1 = "../images/calibration_boards/IMG_6829.jpg"
PATH2 = "../images/calibration_boards/IMG_6829"

FORMAT = "jpg"

SQUARE_SIZE = 25.4
MARKER_SIZE = 0.75 * SQUARE_SIZE

WIDTH = 11 #31
HEIGHT = 8 #23

dictionary = A.getPredefinedDictionary(A.DICT_4X4_1000)
board = A.CharucoBoard_create(11, 8, SQUARE_SIZE, MARKER_SIZE, dictionary)  # 13 in by 9 in
image = board.draw((200*11, 200*8))  # tuple for pixel size
cv.imshow("board", image)
cv.waitKey(0)


def calibrate_charuco(directory_path, image_format, w, h):

    # defines images as everything in given directory path

    arucoParams = A.DetectorParameters_create()
    all_corners, all_ids = [], []

    image = cv.imread(PATH1)

    # Converts images to Grayscale
    gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    cv.imshow("gray", gray)
    cv.waitKey(0)

    corners, ids, rejected = A.detectMarkers(
        gray,
        dictionary,
        parameters=arucoParams
    )

    for (markerCorner, markerID) in zip(corners, ids):
        corners = markerCorner.reshape((4, 2))
        (topLeft, topRight, bottomRight, bottomLeft) = corners

        # Each markerCorner is represented by a list of four (x, y)-coordinates (Line 70).
        # These (x, y)-coordinates represent the top-left, top-right, bottom-right,
        # and bottom-left corners of the ArUco tag (Line 71).
        # Furthermore, the (x, y)-coordinates are always returned in that order.

        topRight = (int(topRight[0]), int(topRight[1]))
        bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
        bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
        topLeft = (int(topLeft[0]), int(topLeft[1]))

        # draws bounding line
        cv.line(image, topLeft, topRight, (255, 0, 0), 10)
        cv.line(image, topRight, bottomRight, (255, 0, 0), 10)
        cv.line(image, bottomRight, bottomLeft, (255, 0, 0), 10)
        cv.line(image, topLeft, bottomLeft, (255, 0, 0), 10)

        # compute and draw center (x,y) of ArUCo marker
        cX = int((topLeft[0] + bottomRight[0]) / 2.0)
        cY = int((topLeft[1] + bottomRight[1]) / 2.0)
        cv.circle(image, (cX, cY), 4, (0, 0, 255), 10)

        # writes ArUCo marker ID onto image
        cv.putText(image, str(markerID),
                   (topLeft[0] + 40, topLeft[1] - 5), cv.FONT_HERSHEY_SIMPLEX,
                   2, (0, 255, 0), 2)
    cv.imshow("image", image)
    cv.waitKey(0)

    rsp, charuco_corners, charuco_ids = A.interpolateCornersCharuco(
        markerCorners=corners,
        markerIds=ids,
        image=gray,
        board=board
    )

    # if charuco board found, collects images/corner points
    if rsp > 20:
        all_corners.append(charuco_corners)
        all_ids.append(charuco_ids)


    ret, mtx, dist, rvecs, tvecs = A.calibrateCameraCharuco(
        charucoCorners=all_corners,
        charucoIds=all_ids,
        board=board,
        imageSize=gray.shape,
        cameraMatrix=None,
        distCoeffs=None
    )


    return [ret, mtx, dist, rvecs, tvecs]


ret, mtx, dist, rvecs, tvecs = calibrate_charuco(PATH2, FORMAT, WIDTH, HEIGHT)

cv_file = cv.FileStorage("calibration_charuco.yaml", cv.FILE_STORAGE_WRITE)
cv_file.write('K', mtx)
cv_file.write('D', dist)
cv_file.release()
print("Calibrated!")
