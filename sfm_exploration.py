import cv2 as cv
import cv2.aruco as A

# initialize matrices from .yaml file created by "calibrate_charuco_board.py"
cv_file = cv.FileStorage('post-calibration/calibration_charuco_52.yaml', cv.FILE_STORAGE_READ)

K = cv_file.getNode("K").mat()  # Camera Matrix
D = cv_file.getNode("D").mat()  # Distortion Matrix

# initialize image path, change image name to target image
PATH = "images/52/mtlb_single_tag/mtlb_single_tag_26.jpg"

# initialize ChArUco board
SQUARE_SIZE = 25.4  # measured in millimeters (mm)
MARKER_SIZE = 0.75 * SQUARE_SIZE  # measured in millimeters (mm)
WIDTH = 11  # measured in inches
HEIGHT = 8  # measured in inches

dictionary = A.getPredefinedDictionary(A.DICT_4X4_1000)  # define ArUCo Dictionary
board = A.CharucoBoard_create(WIDTH, HEIGHT, SQUARE_SIZE, MARKER_SIZE, dictionary)
# image = board.draw((200*11, 200*8))  # shows board

arucoParams = A.DetectorParameters_create()
all_corners, all_ids = [], []  # initialize empty corner and id lists

image = cv.imread(PATH)
image = cv.rotate(image, cv.ROTATE_180)  # rotates image 180 degrees (input images are upside down)
image = cv.undistort(image, K, D, None, K)  # undistort image using K and D matrices
gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)  # convert image to grayscale

corners, ids, rejected = A.detectMarkers(
    gray,
    dictionary,
    parameters=arucoParams
)

new_corners = []
new_ids = []


if len(corners) > 0:

    # Gets and prints rvec and tvec matrices for detected ArUCo tags
    rvec, tvec, markerPoints = A.estimatePoseSingleMarkers(corners[0], 9.45, K, D)

    print("rvec:")
    print(rvec)
    print("tvec:")
    print(tvec)

    # Draw a square around the markers
    A.drawDetectedMarkers(image, corners)

#for (markerCorner, markerID) in zip(new_corners, new_ids):
    #corners = markerCorner.reshape((4, 2))
    #print(corners)
    #(topLeft, topRight, bottomRight, bottomLeft) = corners

    #topRight = (int(topRight[0]), int(topRight[1]))
    #bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
    #bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
    #topLeft = (int(topLeft[0]), int(topLeft[1]))

    #cv.line(image, topLeft, topRight, (255, 0, 0), 10)
    #cv.line(image, topRight, bottomRight, (255, 0, 0), 10)
    #cv.line(image, bottomRight, bottomLeft, (255, 0, 0), 10)
    #cv.line(image, topLeft, bottomLeft, (255, 0, 0), 10)

    # cX = int((((topLeft[0] + topRight[0]) / 2.0) + ((bottomLeft[0] + bottomRight[0]) / 2.0))/2)
    # cY = int((((topLeft[1] + bottomLeft[1]) / 2.0) + ((topRight[1] + bottomRight[1]) / 2.0))/2)
    # cv.circle(image, (cX, cY), 4, (0, 0, 255), 10)

    #cX = int((topLeft[0] + bottomRight[0]) / 2.0)
    #cY = int((topLeft[1] + bottomRight[1]) / 2.0)
    #cv.circle(image, (cX, cY), 4, (0, 0, 255), 10)

    #cv.putText(image, str(markerID),
    #           (topLeft[0] + 40, topLeft[1] - 5), cv.FONT_HERSHEY_SIMPLEX,
    #           2, (0, 0, 0), 2)

    # Draw Axis
    # A.drawAxis(image, K, D, rvec, tvec, 0.01)

# Detect 2D features
# Match 2D features between images

# TODO
# sort for corner markets
# get TVEC and RVEC for each of the 4 corners in array of matrices
# compare those

# challenge: draw camera centers, draw arrow to where camera is pointing in matlab

# Detect Camera Location

# Detect Image Location


# pose estimation

# detectMarkers
# estimatePose
