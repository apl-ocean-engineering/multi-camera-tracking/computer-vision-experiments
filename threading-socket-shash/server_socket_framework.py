import socket
import sys
import os
from struct import unpack
import numpy as np
import cv2 as cv
import threading

# rpi ip : 10.18.178.62

# host ip : 10.19.48.109
# port : 8000


class NewSocket(threading.Thread):

    def __init__(self, host_ip, port):
        threading.Thread.__init__(self)
        self.ip = host_ip
        self.port = port

    def run(self):
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        except socket.error:
            print("failed to create socket")
            sys.exit()
        print("socket created")

        s.bind((self.ip, self.port))
        s.listen(1)
        print("listening...")
        client = s.accept()[0]
        print("connected")

        self.request_image(s, client)

    def request_image(self, s, client):
        print("Type \"image\" to request an image, type \"quit\" to exit")
        x = input()  # different ways of getting input perhaps

        client.send(x.encode())

        if x == "quit":
            print("Stopping Program")
            sys.exit()
        else:
            print("requesting image...")
            self.receive_image(s, client)

    def receive_image(self, s, client):
        byte_stream = client.recv(8)
        (length,) = unpack('>Q', byte_stream)
        data = b''
        while len(data) < length:
            to_read = length - len(data)
            data += client.recv(
                4096 if to_read > 4096 else to_read)

        print("image received")

        open_cv_image = cv.imdecode(np.frombuffer(data, np.uint8), 1)
        file_count = len([name for name in os.listdir('images/two_way_images/')])
        print("File Count is {}".format(file_count))

        # this is where i do edits to the photo #
        cv.imwrite("images/two_way_images/image_{}.jpg".format(file_count), open_cv_image)

        data2 = "received"
        client.send(data2.encode())

        s.close()
        client.close()

        self.run()
