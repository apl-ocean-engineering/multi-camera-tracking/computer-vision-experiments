import socket
import sys
import os
from PIL import Image

import io
import time
import struct
from struct import unpack, pack
import picamera
import numpy as np

def init():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except socket.error:
        print("failed to create socket")
        sys.exit()
    print("socket created")

    host_ip = "10.19.48.109"
    port = 8000
    check_connect(s, host_ip, port)
    

def check_connect(s, host_ip, port):
    location = (host_ip, port)
    result_of_check = s.connect_ex(location)

    if result_of_check == 0:
        print("port open")
        print("socket connected to " + str(host_ip) + " on port " + str(port))
        receive_request(s)
    else:
        print("port not open")
        time.sleep(5.0)
        check_connect(s, host_ip, port)
        
def receive_request(s):
    msg = s.recv(1024).decode()
    if msg.lower() == "quit":
        print("quit")
        init()
    else:
        print("image requested")
        send_image(s)

def send_image(s):
    with picamera.PiCamera() as camera:
        camera.resolution = (640, 480)
        camera.start_preview()
        time.sleep(2)
        
        stream = io.BytesIO()
        
        camera.capture(stream, 'jpeg')
        
        camera.stop_preview()
        
    
    print("image been got")
    
    img_data = np.frombuffer(stream.getvalue(), dtype=np.uint8)
        
    length = pack('>Q', len(img_data))
    
    s.sendall(length)
    s.sendall(img_data)
    
    ds = s.recv(8)
    print(ds)
    init()

init()
