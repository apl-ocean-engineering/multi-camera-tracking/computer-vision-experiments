import cv2.aruco as A
import cv2 as cv
import pathlib
import os

PATH = "images/50"
FORMAT = "jpg"

SQUARE_SIZE = 25.4 #inches
MARKER_SIZE = 0.75 * SQUARE_SIZE #inches

WIDTH = 11
HEIGHT = 8

dictionary = A.getPredefinedDictionary(A.DICT_4X4_1000)
board = A.CharucoBoard_create(11, 8, SQUARE_SIZE, MARKER_SIZE, dictionary)  # 13 in by 9 in


def calibrate_charuco(directory_path, image_format, w, h):

    # defines images as everything in given directory path
    images = pathlib.Path(directory_path).glob(f'*.{image_format}')

    arucoParams = A.DetectorParameters_create()

    all_corners, all_ids = [], []

    for image_name in images:
        image = cv.imread(str(image_name))
        gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
        corners, ids, rejected = A.detectMarkers(
            gray,
            dictionary,
            parameters=arucoParams
        )

        rsp, charuco_corners, charuco_ids = A.interpolateCornersCharuco(
            markerCorners=corners,
            markerIds=ids,
            image=gray,
            board=board
        )
        if rsp > 20:
            all_corners.append(charuco_corners)
            all_ids.append(charuco_ids)

    ret, mtx, dist, rvecs, tvecs = A.calibrateCameraCharuco(
        charucoCorners=all_corners,
        charucoIds=all_ids,
        board=board,
        imageSize=gray.shape,
        cameraMatrix=None,
        distCoeffs=None
    )

    return [ret, mtx, dist, rvecs, tvecs]


ret, mtx, dist, rvecs, tvecs = calibrate_charuco(PATH, FORMAT, WIDTH, HEIGHT)

cv_file = cv.FileStorage("calibration_charuco_50.yaml", cv.FILE_STORAGE_WRITE)
cv_file.write('K', mtx)
cv_file.write('D', dist)
cv_file.release()
