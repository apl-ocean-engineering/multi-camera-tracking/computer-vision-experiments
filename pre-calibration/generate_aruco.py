import numpy as np
import argparse
import cv2 as cv
import sys

# Script format:
# python generate_aruco.py --output <path to output image> --id <intended aruco id> --type <intended dictionary type>

# argument parser ... ?
ap = argparse.ArgumentParser()
ap.add_argument("-o", "--output", required=True,
                help="path to output image containing ArUCo tag")
ap.add_argument("-i", "--id", type=int, required=True,
                help="ID of ArUCo tag to generate")
ap.add_argument("-t", "--type", type=str,
                default="DICT_ARUCO_ORIGINAL",
                help="type of ArUCo tag to generate")
args = vars(ap.parse_args())

# Import ArUCo Dictionary
ARUCO_DICT = {
    "DICT_4X4_50": cv.aruco.DICT_4X4_50,
    "DICT_4X4_100": cv.aruco.DICT_4X4_100,
    "DICT_4X4_250": cv.aruco.DICT_4X4_250,
    "DICT_4X4_1000": cv.aruco.DICT_4X4_1000,
    "DICT_5X5_50": cv.aruco.DICT_5X5_50,
    "DICT_5X5_100": cv.aruco.DICT_5X5_100,
    "DICT_5X5_250": cv.aruco.DICT_5X5_250,
    "DICT_5X5_1000": cv.aruco.DICT_5X5_1000,
    "DICT_6X6_50": cv.aruco.DICT_6X6_50,
    "DICT_6X6_100": cv.aruco.DICT_6X6_100,
    "DICT_6X6_250": cv.aruco.DICT_6X6_250,
    "DICT_6X6_1000": cv.aruco.DICT_6X6_1000,
    "DICT_7X7_50": cv.aruco.DICT_7X7_50,
    "DICT_7X7_100": cv.aruco.DICT_7X7_100,
    "DICT_7X7_250": cv.aruco.DICT_7X7_250,
    "DICT_7X7_1000": cv.aruco.DICT_7X7_1000,
    "DICT_ARUCO_ORIGINAL": cv.aruco.DICT_ARUCO_ORIGINAL,
    "DICT_APRILTAG_16h5": cv.aruco.DICT_APRILTAG_16h5,
    "DICT_APRILTAG_25h9": cv.aruco.DICT_APRILTAG_25h9,
    "DICT_APRILTAG_36h10": cv.aruco.DICT_APRILTAG_36h10,
    "DICT_APRILTAG_36h11": cv.aruco.DICT_APRILTAG_36h11
}

# checks to see if aruco dictionary is supported by opencv
if ARUCO_DICT.get(args["type"], None) is None:
    print("[INFO] ArUCo tag of '{}' is not supported".format(
        args["type"]))
    sys.exit(0)

# sets dictionary based on user input
arucoDict = cv.aruco.Dictionary_get(ARUCO_DICT[args["type"]])

# draws aruco marker based on type of dictionary and id value passed in by user
print("[INFO] generating ArUCo tag type '{}' with ID '{}'".format(
    args["type"], args["id"]))
tag = np.zeros((300, 300, 1), dtype="uint8")
cv.aruco.drawMarker(arucoDict, args["id"], 300, tag, 1)

# shows output image and writes to output file
cv.imwrite(args["output"], tag)
cv.imshow("ArUCo Tag", tag)
cv.waitKey(0)
