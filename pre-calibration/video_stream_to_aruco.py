from imutils.video import VideoStream  # used to access webcam
import cv2 as cv
import time  # used to input delay
import imutils

# FINDS ARUCO TAGS IN VIDEO STREAM

# Initializes key value pairs of possible ArUCo dictionaries
ARUCO_DICT = {
    "DICT_4X4_50": cv.aruco.DICT_4X4_50,
    "DICT_4X4_100": cv.aruco.DICT_4X4_100,
    "DICT_4X4_250": cv.aruco.DICT_4X4_250,
    "DICT_4X4_1000": cv.aruco.DICT_4X4_1000,
    "DICT_5X5_50": cv.aruco.DICT_5X5_50,
    "DICT_5X5_100": cv.aruco.DICT_5X5_100,
    "DICT_5X5_250": cv.aruco.DICT_5X5_250,
    "DICT_5X5_1000": cv.aruco.DICT_5X5_1000,
    "DICT_6X6_50": cv.aruco.DICT_6X6_50,
    "DICT_6X6_100": cv.aruco.DICT_6X6_100,
    "DICT_6X6_250": cv.aruco.DICT_6X6_250,
    "DICT_6X6_1000": cv.aruco.DICT_6X6_1000,
    "DICT_7X7_50": cv.aruco.DICT_7X7_50,
    "DICT_7X7_100": cv.aruco.DICT_7X7_100,
    "DICT_7X7_250": cv.aruco.DICT_7X7_250,
    "DICT_7X7_1000": cv.aruco.DICT_7X7_1000,
    "DICT_ARUCO_ORIGINAL": cv.aruco.DICT_ARUCO_ORIGINAL,
    "DICT_APRILTAG_16h5": cv.aruco.DICT_APRILTAG_16h5,
    "DICT_APRILTAG_25h9": cv.aruco.DICT_APRILTAG_25h9,
    "DICT_APRILTAG_36h10": cv.aruco.DICT_APRILTAG_36h10,
    "DICT_APRILTAG_36h11": cv.aruco.DICT_APRILTAG_36h11
}

# Sets aruco dictionary, parameters
arucoDict = cv.aruco.Dictionary_get(ARUCO_DICT["DICT_4X4_100"])
arucoParams = cv.aruco.DetectorParameters_create()

# Starts video stream
print("[INFO] Starting Video Stream...")
vs = VideoStream(src=0).start()
time.sleep(2.0)  # allows camera sensor to warm up

# Continuously loops through frames of the video capture
while True:
    frame = vs.read()
    frame = imutils.resize(frame, width=1000)

    # Detects markers in image
    (corners, ids, rejected) = cv.aruco.detectMarkers(frame, arucoDict,
                                                      parameters=arucoParams)

    # Check to see if at least one marker was detected
    if len(corners) > 0:
        ids = ids.flatten()

        # Sets coordinate values for corners
        for (markerCorner, markerID) in zip(corners, ids):
            corners = markerCorner.reshape((4, 2))
            (topLeft, topRight, bottomRight, bottomLeft) = corners

            # Each markerCorner is represented by a list of four (x, y)-coordinates (Line 70).
            # These (x, y)-coordinates represent the top-left, top-right, bottom-right,
            # and bottom-left corners of the ArUco tag (Line 71).
            # Furthermore, the (x, y)-coordinates are always returned in that order.

            topRight = (int(topRight[0]), int(topRight[1]))
            bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
            bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
            topLeft = (int(topLeft[0]), int(topLeft[1]))

            # draws bounding line
            cv.line(frame, topLeft, topRight, (0, 255, 0), 2)
            cv.line(frame, topRight, bottomRight, (0, 255, 0), 2)
            cv.line(frame, bottomRight, bottomLeft, (0, 255, 0), 2)
            cv.line(frame, topLeft, bottomLeft, (0, 255, 0), 2)

            # compute and draw center (x,y) of ArUCo marker
            cX = int((topLeft[0] + bottomRight[0]) / 2.0)
            cY = int((topLeft[1] + bottomRight[1]) / 2.0)
            cv.circle(frame, (cX, cY), 4, (0, 0, 255), -1)

            # writes ArUCo marker ID onto image
            cv.putText(frame, str(markerID),
                       (topLeft[0], topLeft[1] - 15), cv.FONT_HERSHEY_SIMPLEX,
                       0.5, (0, 255, 0), 2)
            print("[INFO] ArUCo marker ID: {}".format(markerID))

    # Displays video output with ArUCo marker identified
    cv.imshow("Frame", frame)
    key = cv.waitKey(1) & 0xFF

    # Pressing Q key stops the video capture and the program
    if key == ord("q"):
        break

cv.destroyAllWindows()
vs.stop()
