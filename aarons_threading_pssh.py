import threading
from threading_class_import import NewSocket
import sys
import argparse

from pssh.clients import ParallelSSHClient
from gevent import joinall

# List of pi cameras (hostnames or IP addresses) to connect to
hosts = ['172.31.17.50', '172.31.17.51', '172.31.17.52', '172.31.17.53']

# TODO
# pubkey authentication
# https://serverpilot.io/docs/how-to-use-ssh-public-key-authentication/

# OPAL ARM: 172.31.17.250
# Creates Argument Parser Object to initialize host's ip
# ap = argparse.ArgumentParser()
# ap.add_argument("-s", "--server-ip", required=True,
#                help="IP address of _this machine_ for clients to connect to")
# args = ap.parse_args()

# server_ip = args.server_ip
server_ip = '172.31.17.250'
port_offset = 8000

ids = range(1, 1+len(hosts))

servers = [ NewSocket(i, server_ip, port_offset+i) for i in ids]

client = ParallelSSHClient(hosts, user='pi')

print("Sending file to all clients")
cmds = client.copy_file('threading_client.py', '/tmp/threading_client.py')
joinall(cmds, raise_error=True)

cmd = 'python3 /tmp/threading_client.py -s %s -p ' % server_ip
client.run_command(cmd + '%d',
                    host_args=[port_offset+i for i in ids],
                    stop_on_errors=False )

# One downside ... don't get any debugging output from the clients

for s in servers:
    s.run()

while True:
    print("[INPUT] Type \"image\" to request an image, type \"quit\" to exit")
    x = input()

    if x == "quit":
        print("[STATUS] Stopping Program")
        sys.exit()
    else:
        threads = [ threading.Thread(target=s.request_image) for s in servers]

        for t in threads:
            t.start()

        for t in threads:
            t.join()

        print("Threads finished")
